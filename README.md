# Matdun CV Task
## Task

Implement the following transformations on the given image without using the opencv in-built functions:

- Convert YUV420p image to BGR format. Please refer to the below mentioned link for the conversion formula.
- Apply 3x3 Laplacian filter on the image to generate edges.


We expect you to:
-> save the output of each transformation as png.
-> Each transformation should be implemented optimally. 
-> Use multi-threading to get the optimal performance.


## Files

- img_3024_4032_yuv420p.yuv   -     YUV420p sample image with stride = width.
- OG_img.jpg - reference jpg image to verify the output. 

## Links
  1.  https://docs.opencv.org/3.4/de/d25/imgproc_color_conversions.html

## Submission

Please don't put the assignment solution on git as a public repository. Please send it to me on ashish.arora@matdun.com.